<?php date_default_timezone_set('Asia/Kolkata'); ?>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

   <title>Live chat </title>
   <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<style>
 
html, body{  height: 100%; background-color: #f5f5f5; }
body{ width: 100%; height: 100%;  }
*{ margin: 0px; padding: 0px;  font-family: 'Lato', sans-serif;
outline: none;

}


#chat_data{
   height: 90%;
   background-color: #f5f5f5;
   overflow-y: scroll;
   }

#chat_box{
   position: absolute;
    background-color: ;
    bottom: 2px;
    width: 100%;
}

.input{width:82% ; float: left; margin-left: 5px;} 
.input input{    width: 100%;
    height: 50px;
    border-radius: 50px;
    border: none;
    padding: 0px 20px;
    margin-top: 10px;
    box-shadow: 2px 2px 1px #00000017; } 

.submit_chat{
   width: 15%; float: left;
 }
.submit_chat button{
    height: 50px;
    border: none;
    width: 50px;
    cursor: pointer;
    border-radius: 50%;
    background: #1a4de0;
    color: #fff;
    margin-top: 13px;
        margin-left: 5px;
} 

.messages {
       margin-bottom: 50px;
  position: relative;
  list-style: none;
  padding: 20px 10px 0 10px;
  margin: 0;
  height: 90%;
  overflow-y: scroll;
  
}
.messages .message {
  clear: both;
  overflow: hidden;
  margin-bottom: 20px;
  transition: all 0.5s linear;
  opacity: 0;
}
.messages .message.left .avatar {
  background-color: #f5886e;
  float: left;
}
.messages .message.left .text_wrapper {
  background-color: #ffffff;
  margin-left: 20px;
}
.messages .message.left .text_wrapper::after, .messages .message.left .text_wrapper::before {
  right: 100%;
  border-right-color: #fff;
}
.messages .message.left .text {
    color: #44413f;
}
.messages .message.right .avatar {
  background-color: #fdbf68;
  float: right;
}
.messages .message.right .text_wrapper {
  background-color: #c7eafc;
  margin-right: 20px;
  float: right;
}
.messages .message.right .text_wrapper::after, .messages .message.right .text_wrapper::before {
  left: 100%;
  border-left-color: #c7eafc;
}
.messages .message.right .text {
  color: #45829b;
}
.messages .message.appeared {
  opacity: 1;
}
.messages .message .avatar {
  width: 60px;
  height: 60px;
  border-radius: 50%;
  display: inline-block;
}
.messages .message .text_wrapper {
  display: inline-block;
  padding: 8px;
  border-radius: 6px;
  width: calc(92% - 85px);
  min-width: 100px;
  position: relative;
}
.messages .message .text_wrapper::after, .messages .message .text_wrapper:before {
  top: 18px;
  border: solid transparent;
  content: " ";
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
}
.messages .message .text_wrapper::after {
  border-width: 13px;
  margin-top: 0px;
}
.messages .message .text_wrapper::before {
  border-width: 15px;
  margin-top: -2px;
}
.messages .message .text_wrapper .text {
  font-size: 18px;
  font-weight: 300;
} 

.name { margin-bottom: 10px; color: #505050; font-size: 12px;      font-weight: bolder;}
.name span{ float: right; color: #999; font-size: 10px; }
</style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
   <script src='//cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js'></script>
   
   <script>
      var socket = io('//<?php echo $_SERVER['HTTP_HOST']; ?>:3001');
      var username = '<?php echo $_REQUEST["username"]; ?>';
      var room = '<?php echo $_REQUEST["room"]; ?>';
      var number = '<?php echo $_REQUEST["number"]; ?>';
      var type = '<?php echo $_REQUEST["type"]; ?>';
      function setUsername() {
         socket.emit('setUsername', [username,room]);
      };
      var user;
      // socket.on('connectToRoom',function(data) {
      //    document.body.innerHTML = '';
      //    document.write(data);
      // });
      //<ul class="messages" id = "message-container">\
   //</ul>\
      socket.on('userExists', function(data) {
         document.getElementById('error-container').innerHTML = data;
      });
      socket.on('userSet', function(data) {
         user = data.username;
         document.body.innerHTML = '<div id="chat_data">\
   <ul class="messages" id = "message-old">\
   </ul>\
</div>\
<div id="chat_box"><div class="input"><input type="text" placeholder="Type here" id="message"></div><div class="submit_chat"><button type = "button" name = "button" onclick = "sendMessage()">Send</button></div></div>\
<input type="hidden" name="" id="updated_time" value="0">';
      });
      function sendMessage() {
         var msg = document.getElementById('message').value;
         if(msg) {
            var datt = document.getElementById('updated_time').value;
            //alert(datt);
            socket.emit('msg', {message: msg, user: user, room: room, number: number, type: type, date: datt});
            document.getElementById('message').value = '';
         }
      }
      socket.on('oldmsg', function(data) {
         // console.log(data);
         // console.log(data.user);
         // console.log(user);
         if(user === data.user) {
             //console.log('main');
            // document.getElementById('message-old').innerHTML += '<div><b>' + 
            //    data.user + '</b>: ' + data.message + '</div>'
            //console.log(data);
            var index, len;
            var a = data.result;
            for (index = 0, len = a.length; index < len; ++index) {
                //console.log(a[index].id);
                var datt = document.getElementById('updated_time').value;
                if(number == a[index].number) {
                  document.getElementById('message-old').innerHTML += '<li class="message right appeared">\
                  <div class="text_wrapper">\
                  <div class="name">' + a[index].user_name + '  <span > ' + a[index].added_date + '</span></div>\
                  <div class="text">' + a[index].msg + '</div>\
                  </div>\
                  </li>';
                }else{
                  document.getElementById('message-old').innerHTML += '<li class="message left appeared">\
                  <div class="text_wrapper">\
                  <div class="name">' + a[index].user_name + '  <span > ' + a[index].added_date + '</span></div>\
                  <div class="text">' + a[index].msg + '</div>\
                  </div>\
                  </li>';
                }
                document.getElementById('message-old').scrollTop=document.getElementById('message-old').scrollTop+10000;
                
                
                //document.getElementById('message-old').innerHTML += '<div><b>' + a[index].user_name + '</b>: ' + a[index].msg + '</div>';
            }
            
         }else{
            console.log('else');
         }
      })
      socket.on('newmsg', function(data) {
         if(user) {
            // document.getElementById('message-container').innerHTML += '<div><b>' + 
            //    data.user + '</b>: ' + data.message + '</div>';
            console.log(data);
             if(number == data.number) {
               document.getElementById('message-old').innerHTML += '<li class="message right appeared">\
                  <div class="text_wrapper">\
                     <div class="name">' + data.user + '  <span > ' + data.date + '</span></div>\
                     <div class="text">' + data.message + '</div>\
                  </div>\
               </li>';
             }else{
               document.getElementById('message-old').innerHTML += '<li class="message left appeared">\
                  <div class="text_wrapper">\
                     <div class="name">' + data.user + '  <span > ' + data.date + '</span></div>\
                     <div class="text">' + data.message + '</div>\
                  </div>\
               </li>';
             }
             document.getElementById('message-old').scrollTop=document.getElementById('message-old').scrollTop+10000;
            
         }
      })
      function htmlEncode(value){
        //create a in-memory div, set it's inner text(which jQuery automatically encodes)
        //then grab the encoded contents back out.  The div never exists on the page.
        return $('<div/>').text(value).html();
      }

      function htmlDecode(value){
        return $('<div/>').html(value).text();
      }

      
      setUsername();
      setInterval(function(){ 
        document.getElementById('updated_time').value = moment().utc("+05:30").format("DD-MM-YYYY HH:mm:ss a");
        
      }, 1000);
      window.scrollTo(0,document.body.scrollHeight);
   </script>
    </head>
   <body>
      <div id = "error-container"></div>

   </body>
</html>